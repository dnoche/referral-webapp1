<?php

namespace AppBundle\Utils;

use Symfony\Component\Translation\TranslatorInterface;

class Helper
{
	/** @var TranslatorInterface  */
	protected $trans;

    static public $status = [
    'submitted' => 'Submitted',
    'in_progress' => 'Lead in Progress',
    'closed' => 'Closed Won',
    'cancel' => 'Cancel',
    ];

    static public $statusPayment = [
    'pending' => 'Pending',
    'cancelled' => 'Cancelled',
    'approved' => 'Approved',
    'paid' => 'Paid',
    ];

	/**
	 * Helper constructor.
	 * @param TranslatorInterface $translator
	 */
	public function __construct(TranslatorInterface $translator)
	{
		$this->trans = $translator;
	}

	public function getStatusList()
    {
	    return self::$status;
    }

    public function getStatus($value)
    {
        return self::$status[$value];
    }

    public function getStatusPaymentList()
    {
        return self::$statusPayment;
    }

	/**
	 * @param $month
	 * @return mixed
	 */
	public function monthName($month)
	{
		$listMonth = $this->listMonths();

		return $listMonth[$month];
	}

	public function listMonths()
	{
		return [
				'01' => 'January',
				'02' => 'February',
				'03' => 'March',
				'04' => 'April',
				'05' => 'May',
				'06' => 'June',
				'07' => 'July',
				'08' => 'August',
				'09' => 'September',
				'10' => 'October',
				'11' => 'November',
				'12' => 'December',
		];
	}
}
