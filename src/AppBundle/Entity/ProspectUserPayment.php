<?php

namespace AppBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * ProspectUserPayment
 *
 * @ORM\Table(name="prospect_user_payment")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProspectUserPaymentRepository")
 */
class ProspectUserPayment
{
	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var User $admin
	 *
	 * @ORM\ManyToOne(targetEntity = "AppBundle\Entity\User")
	 * @ORM\JoinColumn(nullable = false)
	 */
	private $admin;

	/**
	 * @var User $user
	 *
	 * @ORM\ManyToOne(targetEntity = "AppBundle\Entity\User")
	 * @ORM\JoinColumn(nullable = false)
	 */
	private $user;

	/**
	 * @var Prospect $prospect
	 *
	 * @ORM\ManyToOne(targetEntity = "AppBundle\Entity\Prospect")
	 * @ORM\JoinColumn(nullable = false)
	 */
	private $prospect;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="amount", type="string", length=50, nullable=true)
	 */
	private $amount;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="month", type="string", length=3, nullable=true)
	 */
	private $month;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="year", type="string", length=4, nullable=true)
	 */
	private $year;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="control_reference", type="string", length=20, nullable=true)
	 */
	private $controlReference;

	/**
	 * @var DateTime
	 *
	 * @ORM\Column(name="created_at", type="datetime")
	 */
	private $createdAt;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="status", nullable=true)
	 */
	private $status;

	/**
	 * ProspectOrder constructor.
	 * @throws \Exception
	 */
	public function __construct()
	{
		$this->createdAt = new \DateTime();
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return User
	 */
	public function getAdmin()
	{
		return $this->admin;
	}

	/**
	 * @param User $admin
	 */
	public function setAdmin($admin)
	{
		$this->admin = $admin;
	}

	/**
	 * @return User
	 */
	public function getUser()
	{
		return $this->user;
	}

	/**
	 * @param User $user
	 */
	public function setUser($user)
	{
		$this->user = $user;
	}

	/**
	 * @return Prospect
	 */
	public function getProspect()
	{
		return $this->prospect;
	}

	/**
	 * @param Prospect $prospect
	 */
	public function setProspect($prospect)
	{
		$this->prospect = $prospect;
	}

	/**
	 * @return string
	 */
	public function getAmount()
	{
		return $this->amount;
	}

	/**
	 * @param string $amount
	 */
	public function setAmount($amount)
	{
		$this->amount = $amount;
	}

	/**
	 * @return string
	 */
	public function getMonth()
	{
		return $this->month;
	}

	/**
	 * @param string $month
	 */
	public function setMonth($month)
	{
		$this->month = $month;
	}

	/**
	 * @return string
	 */
	public function getYear()
	{
		return $this->year;
	}

	/**
	 * @param string $year
	 */
	public function setYear($year)
	{
		$this->year = $year;
	}

	/**
	 * @return DateTime
	 */
	public function getCreatedAt()
	{
		return $this->createdAt;
	}

	/**
	 * @param DateTime $createdAt
	 */
	public function setCreatedAt($createdAt)
	{
		$this->createdAt = $createdAt;
	}

	/**
	 * @return string
	 */
	public function getControlReference()
	{
		return $this->controlReference;
	}

	/**
	 * @param string $controlReference
	 */
	public function setControlReference($controlReference)
	{
		$this->controlReference = $controlReference;
	}

	/**
	 * @return bool
	 */
	public function isStatus()
	{
		return $this->status;
	}

	/**
	 * @param bool $status
	 */
	public function setStatus($status)
	{
		$this->status = $status;
	}

}
