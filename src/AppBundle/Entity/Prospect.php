<?php

namespace AppBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Prospect
 *
 * @ORM\Table(name="prospect")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProspectRepository")
 */
class Prospect
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User $user
     *
     * @ORM\ManyToOne(targetEntity = "AppBundle\Entity\User")
     * @ORM\JoinColumn(nullable = false)
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="employees", type="integer")
     */
    private $employees;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255)
     */
    private $address;


    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=50)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=50)
     */
    private $state;

    /**
     * @var string
     *
     * @ORM\Column(name="zip", type="string", length=20)
     */
    private $zip;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_first_name", type="string", length=50)
     */
    private $contactFirstName;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_last_name", type="string", length=50)
     */
    private $contactLastName;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_title", type="string", length=50)
     */
    private $contactTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_preferred_method_contact", type="string", length=50)
     */
    private $contactPreferredMethodContact;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_phone", type="string", length=50)
     */
    private $contactPhone;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_email", type="string", length=100)
     */
    private $contactEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="customer_type", type="string", length=50)
     */
    private $customerType;

    /**
     * @var boolean
     *
     * @ORM\Column(name="permission_call", type="boolean", nullable=true)
     */
    private $permissionCall;

    /**
     * @var boolean
     *
     * @ORM\Column(name="call_before", type="boolean", nullable=true)
     */
    private $callBefore;

    /**
     * @var string
     *
     * @ORM\Column(name="notes", type="text")
     */
    private $notes;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @Assert\File(
     *      maxSize="5242880",
     * )
     */
    private $attachment;

    /**
     * @var string
     *
     * @ORM\Column(name="order_id", type="string", length=30, nullable=true)
     */
    private $orderId;

    /**
     * @var string
     *
     * @ORM\Column(name="payout_status", type="string", length=50, nullable=true)
     */
    private $payoutStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="payout_amount", type="string", length=50, nullable=true)
     */
    private $payoutAmount;

    /**
     * @var string
     *
     * @ORM\Column(name="mrc", type="string", length=50, nullable=true)
     */
    private $mrc;

    /**
     * @var string
     *
     * @ORM\Column(name="residual_percent", type="string", length=4, nullable=true)
     */
    private $residualPercent;

    /**
     * @var string
     *
     * @ORM\Column(name="contract_attachment", type="string", length=50, nullable=true)
     */
    private $contractAttachment;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="connect_date", type="datetime", nullable=true)
     */
    private $connectDate;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="sold_date", type="datetime", nullable=true)
     */
    private $soldDate;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=50)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="date_of_birth", type="string", length=50, nullable=true)
     */
    private $dateOfBirth;

    /**
     * @var string
     *
     * @ORM\Column(name="social_security", type="string", length=50, nullable=true)
     */
    private $socialSecurity;

    /**
     * @var string
     *
     * @ORM\Column(name="doors", type="string", length=10, nullable=true)
     */
    private $doors;

    /**
     * @var string
     *
     * @ORM\Column(name="window", type="string", length=10, nullable=true)
     */
    private $windows;

    /**
     * @var boolean
     *
     * @ORM\Column(name="alarm_referral", type="boolean", nullable=true)
     */
    private $alarmReferral;

    /**
     * @var string
     *
     * @ORM\Column(name="nutshell_account_id", type="string", length=10, nullable=true)
     */
    private $nutshellAccountId;

    /**
     * @var string
     *
     * @ORM\Column(name="nutshell_lead_id", type="string", length=10, nullable=true)
     */
    private $nutshellLeadId;

    /**
     * @var string
     *
     * @ORM\Column(name="nutshell_contact_id", type="string", length=10, nullable=true)
     */
    private $nutshellContactId;


    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function __toString() {
        return $this->getName();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }


    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getEmployees()
    {
        return $this->employees;
    }

    /**
     * @param int $employees
     */
    public function setEmployees($employees)
    {
        $this->employees = $employees;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param string $zip
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * @return string
     */
    public function getContactFirstName()
    {
        return $this->contactFirstName;
    }

    /**
     * @param string $contactFirstName
     */
    public function setContactFirstName($contactFirstName)
    {
        $this->contactFirstName = $contactFirstName;
    }

    /**
     * @return string
     */
    public function getContactLastName()
    {
        return $this->contactLastName;
    }

    /**
     * @param string $contactLastName
     */
    public function setContactLastName($contactLastName)
    {
        $this->contactLastName = $contactLastName;
    }

    /**
     * @return string
     */
    public function getContactTitle()
    {
        return $this->contactTitle;
    }

    /**
     * @param string $contactTitle
     */
    public function setContactTitle($contactTitle)
    {
        $this->contactTitle = $contactTitle;
    }

    /**
     * @return string
     */
    public function getContactPreferredMethodContact()
    {
        return $this->contactPreferredMethodContact;
    }

    /**
     * @param string $contactPreferredMethodContact
     */
    public function setContactPreferredMethodContact($contactPreferredMethodContact)
    {
        $this->contactPreferredMethodContact = $contactPreferredMethodContact;
    }

    /**
     * @return string
     */
    public function getContactPhone()
    {
        return $this->contactPhone;
    }

    /**
     * @param string $contactPhone
     */
    public function setContactPhone($contactPhone)
    {
        $this->contactPhone = $contactPhone;
    }

    /**
     * @return string
     */
    public function getContactEmail()
    {
        return $this->contactEmail;
    }

    /**
     * @param string $contactEmail
     */
    public function setContactEmail($contactEmail)
    {
        $this->contactEmail = $contactEmail;
    }

    /**
     * @return string
     */
    public function getCustomerType()
    {
        return $this->customerType;
    }

    /**
     * @param string $customerType
     */
    public function setCustomerType($customerType)
    {
        $this->customerType = $customerType;
    }

    /**
     * @return bool
     */
    public function isPermissionCall()
    {
        return $this->permissionCall;
    }

    /**
     * @param bool $permissionCall
     */
    public function setPermissionCall($permissionCall)
    {
        $this->permissionCall = $permissionCall;
    }

    /**
     * @return bool
     */
    public function isCallBefore()
    {
        return $this->callBefore;
    }

    /**
     * @param bool $callBefore
     */
    public function setCallBefore($callBefore)
    {
        $this->callBefore = $callBefore;
    }

    /**
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * @param string $notes
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

		/**
		 * @return string
		 */
		public function getOrderId()
		{
			return $this->orderId;
		}

		/**
		 * @param string $orderId
		 */
		public function setOrderId($orderId)
		{
			$this->orderId = $orderId;
		}

		/**
		 * @return string
		 */
		public function getPayoutStatus()
		{
			return $this->payoutStatus;
		}

		/**
		 * @param string $payoutStatus
		 */
		public function setPayoutStatus($payoutStatus)
		{
			$this->payoutStatus = $payoutStatus;
		}

		/**
		 * @return string
		 */
		public function getPayoutAmount()
		{
			return $this->payoutAmount;
		}

		/**
		 * @param string $payoutAmount
		 */
		public function setPayoutAmount($payoutAmount)
		{
			$this->payoutAmount = $payoutAmount;
		}

		/**
		 * @return DateTime
		 */
		public function getConnectDate()
		{
			return $this->connectDate;
		}

		/**
		 * @param DateTime $connectDate
		 */
		public function setConnectDate($connectDate)
		{
			$this->connectDate = $connectDate;
		}

		/**
		 * @return DateTime
		 */
		public function getSoldDate()
		{
			return $this->soldDate;
		}

		/**
		 * @param DateTime $soldDate
		 */
		public function setSoldDate($soldDate)
		{
			$this->soldDate = $soldDate;
		}

	/**
	 * @return string
	 */
	public function getMrc()
	{
		return $this->mrc;
	}

	/**
	 * @param string $mrc
	 */
	public function setMrc($mrc)
	{
		$this->mrc = $mrc;
	}

	/**
	 * @return string
	 */
	public function getResidualPercent()
	{
		return $this->residualPercent;
	}

	/**
	 * @param string $residualPercent
	 */
	public function setResidualPercent($residualPercent)
	{
		$this->residualPercent = $residualPercent;
	}

	/**
	 * @return string
	 */
	public function getContractAttachment()
	{
		return $this->contractAttachment;
	}

	/**
	 * @param string $contractAttachment
	 */
	public function setContractAttachment($contractAttachment)
	{
		$this->contractAttachment = $contractAttachment;
	}

	/**
	 * @return mixed
	 */
	public function getAttachment()
	{
		return $this->attachment;
	}

	/**
	 * @param mixed $attachment
	 */
	public function setAttachment($attachment)
	{
		$this->attachment = $attachment;
	}

    /**
     * @return string
     */
    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    /**
     * @param string $dateOfBirth
     */
    public function setDateOfBirth($dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;
    }

    /**
     * @return string
     */
    public function getSocialSecurity()
    {
        return $this->socialSecurity;
    }

    /**
     * @param string $socialSecurity
     */
    public function setSocialSecurity($socialSecurity)
    {
        $this->socialSecurity = $socialSecurity;
    }

    /**
     * @return string
     */
    public function getDoors()
    {
        return $this->doors;
    }

    /**
     * @param string $doors
     */
    public function setDoors($doors)
    {
        $this->doors = $doors;
    }

    /**
     * @return string
     */
    public function getWindows()
    {
        return $this->windows;
    }

    /**
     * @param string $windows
     */
    public function setWindows($windows)
    {
        $this->windows = $windows;
    }

    /**
     * @return bool
     */
    public function isAlarmReferral()
    {
        return $this->alarmReferral;
    }

    /**
     * @param bool $alarmReferral
     */
    public function setAlarmReferral($alarmReferral)
    {
        $this->alarmReferral = $alarmReferral;
    }

    /**
     * @return string
     */
    public function getNutshellAccountId()
    {
        return $this->nutshellAccountId;
    }

    /**
     * @param string $nutshellAccountId
     */
    public function setNutshellAccountId($nutshellAccountId)
    {
        $this->nutshellAccountId = $nutshellAccountId;
    }

    /**
     * @return string
     */
    public function getNutshellLeadId()
    {
        return $this->nutshellLeadId;
    }

    /**
     * @param string $nutshellLeadId
     */
    public function setNutshellLeadId($nutshellLeadId)
    {
        $this->nutshellLeadId = $nutshellLeadId;
    }

    /**
     * @return string
     */
    public function getNutshellContactId()
    {
        return $this->nutshellContactId;
    }

    /**
     * @param string $nutshellContactId
     */
    public function setNutshellContactId($nutshellContactId)
    {
        $this->nutshellContactId = $nutshellContactId;
    }
}
