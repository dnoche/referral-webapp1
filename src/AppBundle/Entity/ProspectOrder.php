<?php

namespace AppBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * ProspectOrder
 *
 * @ORM\Table(name="prospect_order")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProspectOrderRepository")
 */
class ProspectOrder
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User $user
     *
     * @ORM\ManyToOne(targetEntity = "AppBundle\Entity\User")
     * @ORM\JoinColumn(nullable = false)
     */
    private $user;

    /**
     * @var Prospect $prospect
     *
     * @ORM\ManyToOne(targetEntity = "AppBundle\Entity\Prospect")
     * @ORM\JoinColumn(nullable = false)
     */
    private $prospect;

    /**
     * @var string
     *
     * @ORM\Column(name="order_id", type="string", length=30, nullable=false)
     */
    private $orderId;

    /**
     * @var string
     *
     * @ORM\Column(name="payout_status", type="string", length=50, nullable=false)
     */
    private $payoutStatus;

		/**
		 * @var string
		 *
		 * @ORM\Column(name="payout_amount", type="string", length=50, nullable=true)
		 */
		private $payoutAmount;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

		/**
		 * @var DateTime
		 *
		 * @ORM\Column(name="connect_date", type="datetime", nullable=true)
		 */
		private $connectDate;

		/**
		 * @var DateTime
		 *
		 * @ORM\Column(name="sold_date", type="datetime", nullable=true)
		 */
		private $soldDate;

	/**
	 * ProspectOrder constructor.
	 * @throws \Exception
	 */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return User
	 */
	public function getUser()
	{
		return $this->user;
	}

	/**
	 * @param User $user
	 */
	public function setUser($user)
	{
		$this->user = $user;
	}

	/**
	 * @return Prospect
	 */
	public function getProspect()
	{
		return $this->prospect;
	}

	/**
	 * @param Prospect $prospect
	 */
	public function setProspect($prospect)
	{
		$this->prospect = $prospect;
	}

	/**
	 * @return string
	 */
	public function getOrderId()
	{
		return $this->orderId;
	}

	/**
	 * @param string $orderId
	 */
	public function setOrderId($orderId)
	{
		$this->orderId = $orderId;
	}

	/**
	 * @return string
	 */
	public function getPayoutStatus()
	{
		return $this->payoutStatus;
	}

	/**
	 * @param string $payoutStatus
	 */
	public function setPayoutStatus($payoutStatus)
	{
		$this->payoutStatus = $payoutStatus;
	}

	/**
	 * @return string
	 */
	public function getPayoutAmount()
	{
		return $this->payoutAmount;
	}

	/**
	 * @param string $payoutAmount
	 */
	public function setPayoutAmount($payoutAmount)
	{
		$this->payoutAmount = $payoutAmount;
	}

	/**
	 * @return DateTime
	 */
	public function getCreatedAt()
	{
		return $this->createdAt;
	}

	/**
	 * @param DateTime $createdAt
	 */
	public function setCreatedAt($createdAt)
	{
		$this->createdAt = $createdAt;
	}

	/**
	 * @return DateTime
	 */
	public function getConnectDate()
	{
		return $this->connectDate;
	}

	/**
	 * @param DateTime $connectDate
	 */
	public function setConnectDate($connectDate)
	{
		$this->connectDate = $connectDate;
	}

	/**
	 * @return DateTime
	 */
	public function getSoldDate()
	{
		return $this->soldDate;
	}

	/**
	 * @param DateTime $soldDate
	 */
	public function setSoldDate($soldDate)
	{
		$this->soldDate = $soldDate;
	}
}
