<?php

namespace AppBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * UserBankInformation
 *
 * @ORM\Table(name="user_bank_information")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserBankInformationRepository")
 */
class UserBankInformation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User $user
     *
     * @ORM\ManyToOne(targetEntity = "AppBundle\Entity\User")
     * @ORM\JoinColumn(nullable = false)
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="type_method", type="string", length=50, nullable=false)
     */
    private $typeMethod;

    /**
     * @var string
     *
     * @ORM\Column(name="information_one", type="string", length=150, nullable=false)
     */
    private $information_one;

		/**
		 * @var string
		 *
		 * @ORM\Column(name="information_two", type="string", length=150, nullable=true)
		 */
		private $information_two;

		/**
		 * @var string
		 *
		 * @ORM\Column(name="information_three", type="string", length=150, nullable=true)
		 */
		private $information_three;

		/**
		 * @var DateTime
		 *
		 * @ORM\Column(name="created_at", type="datetime", nullable=true)
		 */
		private $createdAt;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return User
	 */
	public function getUser()
	{
		return $this->user;
	}

	/**
	 * @param User $user
	 */
	public function setUser($user)
	{
		$this->user = $user;
	}

	/**
	 * @return string
	 */
	public function getTypeMethod()
	{
		return $this->typeMethod;
	}

	/**
	 * @param string $typeMethod
	 */
	public function setTypeMethod($typeMethod)
	{
		$this->typeMethod = $typeMethod;
	}

	/**
	 * @return string
	 */
	public function getInformationOne()
	{
		return $this->information_one;
	}

	/**
	 * @param string $information_one
	 */
	public function setInformationOne($information_one)
	{
		$this->information_one = $information_one;
	}

	/**
	 * @return string
	 */
	public function getInformationTwo()
	{
		return $this->information_two;
	}

	/**
	 * @param string $information_two
	 */
	public function setInformationTwo($information_two)
	{
		$this->information_two = $information_two;
	}

	/**
	 * @return DateTime
	 */
	public function getCreatedAt()
	{
		return $this->createdAt;
	}

	/**
	 * @param DateTime $createdAt
	 */
	public function setCreatedAt($createdAt)
	{
		$this->createdAt = $createdAt;
	}

	/**
	 * @return string
	 */
	public function getInformationThree()
	{
		return $this->information_three;
	}

	/**
	 * @param string $information_three
	 */
	public function setInformationThree($information_three)
	{
		$this->information_three = $information_three;
	}
}
