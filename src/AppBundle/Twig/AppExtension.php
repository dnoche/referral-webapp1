<?php
// src/AppBundle/Twig/AppExtension.php
namespace AppBundle\Twig;

class AppExtension extends \Twig_Extension
{
	static public $status = array(
			'submitted' => 'Submitted',
			'in_progress' => 'Lead in Progress',
			'closed' => 'Closed Won',
			'cancel' => 'Cancel',
			'pending' => 'Pending',
			'cancelled' => 'Cancelled',
			'approved' => 'Approved',
			'paid' => 'Paid',
	);

	static public $paymentMethod = array(
			'check' => 'Check',
			'deposit' => 'Direct Deposit',
	);

	static public $months =  [
				'01' => 'January',
				'02' => 'February',
				'03' => 'March',
				'04' => 'April',
				'05' => 'May',
				'06' => 'June',
				'07' => 'July',
				'08' => 'August',
				'09' => 'September',
				'10' => 'October',
				'11' => 'November',
				'12' => 'December',
		];

	public function getFilters()
	{
		return array(
				new \Twig_SimpleFilter('price', array($this, 'priceFilter')),
				new \Twig_SimpleFilter('pad', array($this, 'padFilter')),
				new \Twig_SimpleFilter('referral_status', array($this, 'statusFilter')),
				new \Twig_SimpleFilter('payment_method', array($this, 'paymentMethodFilter')),
				new \Twig_SimpleFilter('month', array($this, 'monthFilter')),
		);
	}

	public function priceFilter($number, $decimals = 0, $decPoint = '.', $thousandsSep = ',')
	{
		$price = number_format($number, $decimals, $decPoint, $thousandsSep);
		$price = '$'.$price;

		return $price;
	}

	public function padFilter($string)
	{
		return str_pad($string, 8, '0', STR_PAD_LEFT);
	}

	public function statusFilter($value)
	{
		return self::$status[$value];
	}

	public function paymentMethodFilter($value)
	{
		return self::$paymentMethod[$value];
	}

    public function monthFilter($value)
    {
        return self::$months[$value];
    }
}