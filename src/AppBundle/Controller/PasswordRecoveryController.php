<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\ChangePasswordType;
use AppBundle\Form\Model\ChangePassword;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class PasswordRecoveryController extends Controller
{
	/**
	 * @Route("/forgot", name="app_password_recovery_one")
	 * @Template("@App/passwordrecovery/index.html.twig")
	 * @param Request $request
	 * @return array|Response
	*/
	public function indexAction(Request $request)
	{
			if ($request->isMethod('post')) {
				$em = $this->getDoctrine()->getManager();
				$email = $request->request->get("email");
				$user = $em->getRepository('AppBundle:User')->findOneBy([ 'email'=> $email]);

				if ( $user == null ){
					$this->get('session')->getFlashBag()->add('error', "We could not find a user associated with the email address entered, please check the email entered.");
				}else{
					$message = "We have sent an email to the address " . $user->getEmail() . " so you can reset your password.";
					$this->get('session')->getFlashBag()->add('success', $message);
					$this->sendMailRequestNewPasswordAction($user);
				}
			}

      return [

      ];
	}


	public function sendMailRequestNewPasswordAction(User $user){

			$message = \Swift_Message::newInstance()
				->setSubject('Password Reset!')
					->setFrom(array('sales@inteltechnologygroup.com' => $this->getParameter('name_application') ))
				->setTo($user->getEmail())
				->setContentType("text/html")
				->setBody($this->renderView('@App/Emails/change-pasword.html.twig', [
						'user_id' => $user->getId(),
						'email'    => $user->getEmail()
				]));

			$this->get('mailer')->send($message);
	}

	/**
	 * @Route("/forgot/{user_id}/update-password/{email}", name="app_password_update")
	 * @Template("@App/passwordrecovery/update.html.twig")
	 * @param Request $request
	 * @return array|Response
	 */
	public function updateAction(Request $request) {
		$em = $this->getDoctrine()->getManager();
		$user = $em->getRepository('AppBundle:User')->find($request->attributes->get('user_id'));

		if ( $user->getEmail() != $request->attributes->get('email') ) {
			$message = "We have not found a user with the email address: " . $request->attributes->get('email') . ",  if you think it is an error, please contact one of the Administrators.";
			$this->get('session')->getFlashBag()->add('error', $message);
		}
//{{ include('_flash_messages.html.twig') }}
		return [];
	}

	/**
	 * @Route("/forgot/{user_id}/update-password-proccess", name="app_password_update_proccess")
	 * @Template("@App/passwordrecovery/update.html.twig")
	 * @param Request $request
	 * @return array|Response
	 */
	public function processAction(Request $request) {

		if($this->getUser() != null){
			$this->get('security.token_storage')->setToken(null);
			$request->getSession()->invalidate();
		}

		try {
			$userId = $request->attributes->get('user_id');
			$em = $this->getDoctrine()->getManager();
			$newPassword = $request->request->get("new_password");
			$user = $em->getRepository('AppBundle:User')->find($userId);

			$user->setPassword($newPassword);
			$user = $this->encryptPassword($user);
			$em->persist($user);
			$em->flush();

			$message = "Password updated successfully! <br> In a moment you will be redirected to the login page";
			$this->get('session')->getFlashBag()->add('success', $message);
			$code = 200;
		}catch(\Exception $e){
			$message = "An error has occurred " . $e-> getMessage (). "; Please report it to the Administrator";
			$this->get('session')->getFlashBag()->add('error', $message);
			$code = 500;
		}

		return ['code' => $code];
	}

	protected function encryptPassword(User $entity){
		$factory = $this->get('security.encoder_factory');
		$encoder = $factory->getEncoder($entity);
		$password = $encoder->encodePassword($entity->getPassword(), $entity->getSalt());
		$entity->setPassword($password);

		return $entity;
	}
}
