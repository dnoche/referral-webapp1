<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
	    if (
			    false === $this->get('security.authorization_checker')->isGranted('ROLE_USER') ||
			    false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')
	    ) {
		    return $this->redirectToRoute('login');
	    }

        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/terms", name="app_termns")
     * @return array
     * @Template()
     */
    public function termsAction(){

        return [];
    }

    /**
     * @Route("/privacy-policys", name="app_privacy")
     * @return array
     * @Template()
     */
    public function privacyAction(){

        return [];
    }
}
