<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AuthController
 * @package AppBundle\Controller
 * @Route("/home")
 */
class HomeController extends Controller
{
	/**
	 * @Route("/", name="app_home")
	 * @Template()
	 * @return array
	 */
	public function indexAction()
	{
		$em = $this->getDoctrine()->getManager();
		$earnedMonth = 0;
		$users = 0;
		$submitted = 0;
		$closed = 0;
		$paid = 0;
		$pending = 0;
		$cancelled = 0;
		$earnedPreviousMonth =0;
		$earnedYear = 0;
		$previousMonth = date( 'm', strtotime("first day of previous month"));
		$totalResidualPayment = 0;
		$totalSalesYear = 0;
		$totalSalesCurrentMonth = 0;
		$totalSalesPreviousMonth = 0;
		$user = $this->getUser()->getId();

		if ( true === $this->get('security.authorization_checker')->isGranted('ROLE_USER') ) {
			$submitted = $this->getDoctrine()->getRepository('AppBundle:Prospect')
					->countByUserAndStatus($user, 'submitted');
			$closed = $this->getDoctrine()->getRepository('AppBundle:Prospect')
					->countByUserAndStatus($user, 'closed');
			$paid = $this->getDoctrine()->getRepository('AppBundle:Prospect')
					->countByUserAndPayoutStatus($user, 'paid');
			$pending = $this->getDoctrine()->getRepository('AppBundle:Prospect')
					->countByUserAndPayoutStatus($user, 'pending');
			$earnedMonth = $this->getDoctrine()->getRepository('AppBundle:ProspectUserPayment')
			    ->earnedInYearAndOrMonth($user, date('Y'), date('m'));


	    $yearPreviousMonth = date( 'Y', strtotime("first day of previous month"));
	    $earnedPreviousMonth = $this->getDoctrine()->getRepository('AppBundle:ProspectUserPayment')
	        ->earnedInYearAndOrMonth($user, $yearPreviousMonth, $previousMonth);
	    $earnedYear = $this->getDoctrine()->getRepository('AppBundle:ProspectUserPayment')
	        ->earnedInYearAndOrMonth($user, date('Y'));
		}

		if ( true === $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') ) {
			$pending = $em->getRepository('AppBundle:Prospect')
					->countTotalPayoutStatus('pending');

			$cancelled = $em->getRepository('AppBundle:Prospect')
					->countTotalPayoutStatus('cancelled');

			$totalResidualPayment = $em->getRepository('AppBundle:ProspectUserPayment')
					->totalResidualPayments();

			$totalSalesYear = $em->getRepository('AppBundle:Prospect')
					->totalSalesByDate(date( 'Y'));

			$totalSalesCurrentMonth = $em->getRepository('AppBundle:Prospect')
					->totalSalesByDate(date( 'Y'), date('m'));

			$totalSalesPreviousMonth = $em->getRepository('AppBundle:Prospect')
					->totalSalesByDate(date( 'Y'), $previousMonth);

			$users = $this->getDoctrine()->getRepository('AppBundle:User')->findAll();
			$users = count($users);
		}

		return [
				'users' => $users > 0 ? $users - 1 : $users,
		    'previousMonth' => $previousMonth,
		    'status' => [
				'submitted' => $submitted,
				'closed'  => $closed,
				'paid'  => $paid
		    ],
        'payout' => [
            'pending' => $pending,
	          'cancelled' => $cancelled,
	          'totalResidualPayment' => $totalResidualPayment,
	          'totalSalesYear' => $totalSalesYear,
	          'totalSalesCurrentMonth' => $totalSalesCurrentMonth,
	          'totalSalesPreviousMonth' => $totalSalesPreviousMonth,
        ],
        'earned' => [
            'month' => $earnedMonth ?: 0,
            'previousMonth' => $earnedPreviousMonth ?: 0,
            'earnedYear' => $earnedYear ?: 0
        ]
		];
	}

	protected function mailTest()
	{
		$message = \Swift_Message::newInstance()
				->setSubject('Thank you for registering with ITG Residuals Website!')
				->setFrom(array('sales@inteltechnologygroup.com' => $this->getParameter('name_application') ))
				->setTo($this->getParameter('mail_test'))
				->setContentType("text/html")
				->setBody(
						$this->renderView(
								'@App/Emails/registration.html.twig',
								array('name' => 'Matias')
						),
						'text/html'
				);
		$this->get('mailer')->send($message);
	}
}
