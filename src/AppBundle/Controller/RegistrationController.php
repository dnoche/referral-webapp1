<?php

namespace AppBundle\Controller;

use AppBundle\Form\UserType;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationController extends Controller
{
    /**
     * @Route("/register", name="user_registration")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function registerAction(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        // 1) build the form
        $user = new User();
        $user->setUsername('new-user');
        $user->setRole('ROLE_USER');
        $user->disabled();
        $form = $this->createForm(UserType::class, $user);

        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            // 3) Encode the password (you could also do this via Doctrine listener)
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            $user->setUsername($user->getEmail());
	          $user->disabled();
            // 4) save the user!
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->mail($user);
            // like sending them an email
            $msg = 'You have successfully registered! We will review your application! Thank you for registering with www.itgresiduals.com';
            $this->get('session')->getFlashBag()->add('success', $msg);

            return $this->redirectToRoute('homepage');
        }

        return $this->render(
            '@App/registration/register.html.twig',
            array('form' => $form->createView())
        );
    }

    /**
     * @param User $user
     */
    protected function mail(User $user)
    {
        $message = \Swift_Message::newInstance()
                ->setSubject("Thank you for registering with ITG Residual's Website!")
                ->setFrom(array('sales@inteltechnologygroup.com' => $this->getParameter('name_application') ))
                ->setTo($user->getEmail())
//                ->setTo($this->getParameter('mail_test'))
                ->setContentType("text/html")
                ->setBody(
                        $this->renderView(
                                '@App/Emails/registration.html.twig',
                                array('name' => $user->getFullName())
                        ),
                        'text/html'
                );

        $messageAdmin = \Swift_Message::newInstance()
                ->setSubject("New user has been added to ITG Residual's Website")
                ->setFrom(array('sales@inteltechnologygroup.com' => $this->getParameter('name_application') ))
                ->setTo($this->getParameter('mail_company'))
                ->setContentType("text/html")
                ->setBody(
                        $this->renderView(
                                '@App/Emails/Admin/registration.html.twig',
                                array('user' => $user)
                        ),
                        'text/html'
                );
        $this->get('mailer')->send($message);
        $this->get('mailer')->send($messageAdmin);
    }
}