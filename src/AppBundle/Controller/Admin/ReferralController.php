<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Prospect;
use AppBundle\Entity\ProspectHistory;
use AppBundle\Entity\ProspectOrder;
use AppBundle\Entity\User;
use AppBundle\Form\ProspectType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class ReferralController
 * @package AppBundle\Controller
 * @Route("/admin/referral")
 */
class ReferralController extends Controller
{
	/**
	 * @Security("has_role('ROLE_ADMIN')")
	 * @Route("/list", name="admin_referral_list")
	 * @Template()
	 * @param Request $request
	 * @return array
	 */
	public function listAction(Request $request)
	{
		if ($request->isMethod('post')) {
		    $data = $request->request->all();

			#Filter by user
			$user = $data['user-filter'];
			$status = $data['status-filter'];
			$paymentStatus = $data['payment-status-filter'];
			$filterWhere = [];

			if ($user) {
                $filterWhere['user'] = $user;
            }

            if ($status) {
                $filterWhere['status'] = $status;
            }

            if ($paymentStatus) {
                $filterWhere['payoutStatus'] = $paymentStatus;
            }

			$list = $this->getDoctrine()->getRepository('AppBundle:Prospect')
					->loadFilter($filterWhere);

            if ( isset($data['export']) ) {
                $delimiter = ",";
                $filename = "referrals_" . date('Y-m-d') . ".csv";

                //create a file pointer
                $f = fopen('php://memory', 'w');

                //set column headers
                $fields = array('ID', 'Submitter', 'Business Name', 'Date created', 'Status', 'OrderID', 'Payout Status');
                fputcsv($f, $fields, $delimiter);

                //output each row of the data, format line as csv and write to file pointer
                foreach ($list as $row) {
                    $status = ($row['status'] !== '') ? $this->get('app.utils_helper')->getStatus($row['status']) : '';
                    $lineData = array(
                        $row['id'],
                        $row['user_firstName'],
                        $row['company'],
                        $row['createdAt']->format('d/m/Y'),
                        $status,
                        $row['orderId'],
                        $row['payout_status']);
                    fputcsv($f, $lineData, $delimiter);
                }

                //move back to beginning of file
                fseek($f, 0);

                //set headers to download file rather than displayed
                header('Content-Type: text/csv');
                header('Content-Disposition: attachment; filename="' . $filename . '";');

                //output all remaining data on a file pointer
                fpassthru($f);
                fclose($f);
                exit();
            }
		} else {
			$list = $this->getDoctrine()->getRepository('AppBundle:Prospect')->loadFilter();
		}

		$users = $this->getDoctrine()->getRepository('AppBundle:User')
				->findBy(['role' => 'ROLE_USER']);

		return [
            'list' => $list,
            'users' => $users,
            'status' => $this->get('app.utils_helper')->getStatusList(),
            'statusPayment' => $this->get('app.utils_helper')->getStatusPaymentList(),
		];
	}

    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("/view/{id}", name="admin_referral_view")
     * @Route("/view/{id}/make-action/{action}", name="admin_referral_make_action")
     * @Template()
     * @param Request $request
     * @return array|RedirectResponse
     * @throws \Exception
     */
	public function viewAction(Request $request)
	{
		$em = $this->getDoctrine()->getManager();
		$prospect = $this->getDoctrine()->getRepository('AppBundle:Prospect')
				->find($request->attributes->get('id'));
		$action = $request->attributes->get('action', null);

		if ($action === 'order') {
//				$prospect->setPayoutStatus('pending');
//				$order->setUser($this->getUser());
//				$order->setPayoutStatus('pending');
//				$em->persist($order);
//				$em->flush();

		}

		if ($request->isMethod('post')) {

			$data = $request->request->all();
			$history = new ProspectHistory();
			$history->setProspect($prospect);
			$prospect->setStatus($data['status']);

			if ( $data['status'] == 'closed' ) {
			    $prospect->setConnectDate(new \DateTime('now'));
            }

			$history->setStatus($data['status']);
			$history->setNote($data['message']);
			$history->setUser($this->getUser());
			$em->persist($prospect);
			$em->persist($history);
			$em->flush();

			if ( $data['status'] == 'closed' ) {
			    $this->notificationPendingInstallation($prospect);
            }

			return $this->redirectToRoute('admin_referral_view', ['id' => $prospect->getId()]);
		}

		$status = [
				'submitted' => 'Submitted',
				'in_progress' => 'Lead in Progress',
				'closed' => 'Closed Won',
				'cancel' => 'Cancel',
		];

		$statusPayment = [
				'pending' => 'Pending',
				'cancelled' => 'Cancelled',
				'approved' => 'Approved',
				'paid' => 'Paid',
		];

		$history = $this->getDoctrine()->getRepository('AppBundle:ProspectHistory')
				->findBy(['prospect' => $prospect->getId()]);
		return [
//				'form' => $form->createView(),
				'prospect' => $prospect,
				'status' => $status,
				'statusPayment' => $statusPayment,
				'history' => $history
		];
	}

    protected function notificationPendingInstallation(Prospect $prospect)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject("ITG Referral Update")
            ->setFrom(array( $this->getParameter('mail_company') => $this->getParameter('name_application') ))
//            ->setTo($this->getParameter('mail_test'))
            ->setTo($prospect->getUser()->getEmail())
            ->setContentType("text/html")
            ->setBody(
                $this->renderView(
                    '@App/Emails/Admin/referral_close_won.html.twig',
                    array('referral' => $prospect)
                ),
                'text/html'
            );

        $this->get('mailer')->send($message);
    }

    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("/order/{id}", name="admin_referral_order")
     * @Template()
     * @param Request $request
     * @return array|RedirectResponse
     * @throws \Exception
     */
	public function orderAction(Request $request)
	{
		$em = $this->getDoctrine()->getManager();
		$prospect = $this->getDoctrine()->getRepository('AppBundle:Prospect')
				->find($request->attributes->get('id'));

		if ($request->isMethod('post')) {
			$data = $request->request->all();
			$file  = $request->files->get('attach-order');

			if ($file) {
				$fileName = 'order-contract-referral-' . $prospect->getId().'.'.$file->guessExtension();
				$file->move(
						$this->getParameter('referrals_directory'),
						$fileName
				);
				$prospect->setContractAttachment($fileName);
			}

			$prospect->setOrderId($data['order-id']);
			$prospect->setPayoutStatus($data['status-payment']);

			if ($data['status-payment'] === 'paid') {
			    $prospect->setSoldDate(new \DateTime('now'));
            }

			$prospect->setMrc($data['mrc']);
			$prospect->setResidualPercent($data['residual-percent']);

			#Calculate amount
			if ($data['mrc'] > 0 && $data['residual-percent'] > 0) {
				$amount =  ($data['mrc'] * $data['residual-percent']) / 100;
				$prospect->setPayoutAmount($amount);
			}

			$em->persist($prospect);
			$em->flush();

			if ($data['status-payment'] == 'paid') {
				$this->get('session')->getFlashBag()->add('success', "Order in paid status. Now enter the user's payment");
				return $this->redirectToRoute('admin_referral_payment_create', ['id' => $prospect->getId()]);
			}
		}

		$this->get('session')->getFlashBag()->add('success', 'Order created');
		return $this->redirectToRoute('admin_referral_view', ['id' => $prospect->getId()]);
	}

	/**
	 * @Security("has_role('ROLE_ADMIN')")
	 * @Route("/{id}/delete", name="admin_referral_delete")
	 * @param Request $request
	 * @return JsonResponse
	 * @throws \Exception
	 */
	public function remove(Request $request)
	{
		$em = $this->getDoctrine()->getManager();

		try {

			$prospect = $this->getDoctrine()->getRepository('AppBundle:Prospect')
					->find($request->attributes->get('id'));
			# remove payments
			$em->getRepository('AppBundle:ProspectUserPayment')
					->removeByProspect($prospect->getId());
			# remove history
			$em->getRepository('AppBundle:ProspectHistory')
					->removeByProspect($prospect->getId());

			$em->remove($prospect);
			$em->flush();

			$result = ['code' => 200,'success' => 'User removed'];
		} catch (\Exception $e) {
			$result = [
					'code' => 400,
					'error' => $e->getMessage()
			];
		}

		return new JsonResponse($result, 200);
	}
}
