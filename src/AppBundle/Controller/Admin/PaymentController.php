<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Prospect;
use AppBundle\Entity\ProspectUserPayment;
use AppBundle\Utils\NutshellApi;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class PaymentController
 * @package AppBundle\Controller
 * @Route("/admin/referral/payment")
 */
class PaymentController extends Controller
{
    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("/nutshell-list", name="admin_nutshell_contacts")
     * @Template()
     * @return void
     * @throws \Exception
     */
    public function nutshellContacts() {


        $api = new NutshellApi('inteltechnologygroup@gmail.com', 'e1063459ed526aa83cc72092befbe8493a5754e2');

//        https://github.com/nutshellcrm/nutshell-api-php/blob/master/examples/create.php
        // Create a new account that includes the contact we just added
//        nombre negocio
//        direccion
//        tel
//        email
//        notas


//        $params = array(
//            'account' => array(
//                'name' => 'Test Arbor Medical LLC',
//                'industryId' => 1,
//                'url' => array(
//                    'http://example.com',
//                    'http://suppliers.example.com',
//                ),
//                'phone' => array(
//                    '734-234-9990',
//                ),
//                'contacts' => array(
//                    array(
//                        'id' => 103,
//                        'relationship' => 'Purchasing Manager'
//                    ),
//                ),
//                'address' => array(
//                    'office' => array(
//                        'address_1'  => '220 Depot St',
//                        'city'       => 'Ann Arbor',
//                        'state'      => 'MI',
//                        'postalCode' => '48104',
//                    ),
//                ),
//            ),
//        );
//        $newAccount = $api->newAccount($params);
//        $newAccountId = $newAccount->id;

//        echo "<pre>";
//        print_r($newAccountId);
//        die;

$account =  575;
        // Finally, create a lead that includes the account we just added
$params = array(
    'lead' => array(
        'primaryAccount' => array('id' => $account),
        'contacts' => array(
            array(
                'relationship' => 'First Contact',
                'id'           => 103,
            ),
        ),
//        'products' => array(
//            array(
//                'relationship' => '',
//                'quantity'     => 15,
//                'price'        => array(
//                    'currency_shortname' => 'USD',
//                    'amount'   => 1000,
//                ),
//                'id'           => 4,
//            ),
//        ),
//        'sources' => array(
//            array('id' => 2),
//        ),
//        'assignee' => array(
//            'entityType' => 'Teams',
//            'id' => 1000,
//        ),
    ),
);
$result = $api->newLead($params);
var_dump($result);

echo "\n";
die;


        // Retrieve a contact
//        echo "getContact example\n------------------------------------------------\n";
//        $result = $api->call('getContact', []);
//        var_dump($result);




// Find contacts attached to lead #1209
//        echo "\nfindContacts example\n------------------------------------------------\n";
//        $params = array(
//            'query' => array(
//                'leadId' => 1209,
//            ),
//        );
//        $result = $api->findContacts($params);
//        var_dump($result);
//
//        echo "\n";
    }

	/**
	 * @Security("has_role('ROLE_ADMIN')")
	 * @Route("/{id}/list", name="admin_referral_payment_list")
	 * @Template()
	 * @param Request $request
	 * @return array|RedirectResponse
	 */
	public function indexAction(Request $request)
	{
		$em = $this->getDoctrine()->getManager();
		$prospect = $em->getRepository('AppBundle:Prospect')
				->find($request->attributes->get('id'));

		$payments = $em->getRepository('AppBundle:ProspectUserPayment')
				->findBy(['prospect' => $prospect->getId()]);

		$listMonth = $this->get('app.utils_helper')->listMonths();
		$listYear = [
				'2020' => 2020,
				'2021' => 2021
		];

		return [
				'prospect' => $prospect,
				'payments' => $payments,
				'listYear' => $listYear,
				'listMonth' => $listMonth,
		];
	}

	/**
	 * @Security("has_role('ROLE_ADMIN')")
	 * @Route("/payment/{id}/create", name="admin_referral_payment_create")
	 * @Template()
	 * @param Request $request
	 * @return array|RedirectResponse
	 * @throws \Exception
	 */
	public function createAction(Request $request)
	{
		$em = $this->getDoctrine()->getManager();
		$prospect = $em->getRepository('AppBundle:Prospect')
				->find($request->attributes->get('id'));

		if ($request->isMethod('POST')) {
				$data = $request->request->all();

				if ( $data['amount'] <= 0) {
					$this->get('session')->getFlashBag()->add('error', 'The amount to be paid cannot be 0');
					return $this->redirectToRoute('admin_referral_payment_create', ['id' => $prospect->getId()]);
				}

				if ($em->getRepository('AppBundle:ProspectUserPayment')
					->findBy([
							'prospect' => $prospect->getId(),
							'year' => $data['year'],
							'month' => $data['month'],
					])) {

					$this->get('session')->getFlashBag()->add('error', 'Month has already been paid');
					return $this->redirectToRoute('admin_referral_payment_list', ['id' => $prospect->getId()]);
				}

				$payment = new ProspectUserPayment();
				$payment->setAdmin($this->getUser());
				$payment->setUser($prospect->getUser());
				$payment->setProspect($prospect);
				$payment->setAmount($data['amount']);
				$payment->setControlReference($data['control_reference']);
				$payment->setStatus($data['status']);
				$payment->setYear($data['year']);
				$payment->setMonth($data['month']);

				$em->persist($payment);
				$em->flush();

				$this->mailNotificationAboutPayment($prospect, $data['amount']);

				$this->get('session')->getFlashBag()->add('success', 'Month paid');
				return $this->redirectToRoute('admin_referral_payment_list', ['id' => $prospect->getId()]);
		}

		$listMonth = $this->get('app.utils_helper')->listMonths();
		$listYear = ['2020' => 2020, '2021' => 2021];
		$status = ['1' => 'Paid', '0' => 'Pending'];

		return [
				'prospect' => $prospect,
				'listYear' => $listYear,
				'listMonth' => $listMonth,
				'status' => $status
		];
	}

    /**
     * @param Prospect $prospect
     * @param $amount
     */
    protected function mailNotificationAboutPayment(Prospect $prospect, $amount)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject("ITG Referral Update")
            ->setFrom(array( $this->getParameter('mail_company') => $this->getParameter('name_application') ))
            ->setTo($this->getParameter('mail_test'))
//            ->setTo($prospect->getUser()->getEmail())
            ->setContentType("text/html")
            ->setBody(
                $this->renderView(
                    '@App/Emails/Admin/referral_payment_referral_month.html.twig',
                    array('referral' => $prospect, 'amount' => $amount)
                ),
                'text/html'
            );

        $this->get('mailer')->send($message);
    }

	/**
	 * @Security("has_role('ROLE_ADMIN')")
	 * @Route("/payment/{id}/edit", name="admin_referral_payment_edit")
	 * @Template()
	 * @param Request $request
	 * @return array|RedirectResponse
	 * @throws \Exception
	 */
	public function editAction(Request $request)
	{
		$em = $this->getDoctrine()->getManager();
		$payment = $em->getRepository('AppBundle:ProspectUserPayment')
				->find($request->attributes->get('id'));
		$amount = 0;
		if ($payment->getProspect()->getMrc() > 0 && $payment->getProspect()->getResidualPercent() > 0) {
			$amount =  ($payment->getProspect()->getMrc() * $payment->getProspect()->getResidualPercent()) / 100;
		}

		if ($request->isMethod('POST')) {
			$data = $request->request->all();
			$prospect = $em->getRepository('AppBundle:Prospect')
					->find($payment->getProspect());

			$payment->setAmount($data['amount']);
			$payment->setControlReference($data['control_reference']);
			$payment->setStatus($data['status']);
			$payment->setYear($data['year']);
			$payment->setMonth($data['month']);

			$em->persist($payment);
			$em->flush();

			$this->get('session')->getFlashBag()->add('success', 'Month updated');
			return $this->redirectToRoute('admin_referral_payment_list', ['id' => $payment->getProspect()->getId() ]);
		}

		$listMonth = $this->get('app.utils_helper')->listMonths();
		$listYear = [
				'2020' => 2020,
				'2021' => 2021
		];

		$status = [
				'1' => 'Paid',
				'0' => 'Pending',
		];

		return [
				'prospect' => $payment->getProspect(),
				'payment' => $payment,
				'listYear' => $listYear,
				'listMonth' => $listMonth,
				'amount' => $amount,
				'status' => $status
		];
	}

	/**
	 * @Security("has_role('ROLE_ADMIN')")
	 * @Route("/payment/{id}/delete", name="admin_referral_payment_delete")
	 * @Template()
	 * @param Request $request
	 * @return JsonResponse
	 * @throws \Exception
	 */
	public function remove(Request $request)
	{
		try {
			$payment = $this->getDoctrine()->getRepository('AppBundle:ProspectUserPayment')
					->find($request->attributes->get('id'));

			$this->getDoctrine()->getManager()->remove($payment);
			$this->getDoctrine()->getManager()->flush();
			$result = ['code' => 200,'success' => 'Payment removed'];
		} catch (\Exception $e) {
			$result = [
					'code' => 400,
					'error' => $e->getMessage()
			];
		}

		return new JsonResponse($result, 200);
	}
}