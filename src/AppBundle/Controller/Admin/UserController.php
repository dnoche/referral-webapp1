<?php

namespace AppBundle\Controller\Admin;
use AppBundle\Entity\User;
use AppBundle\Entity\UserBankInformation;
use AppBundle\Form\BankInformationType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class UserController
 * @package AppBundle\Controller
 * @Route("/admin/users")
 */
class UserController extends Controller
{
	/**
	 * @Security("has_role('ROLE_ADMIN')")
	 * @Route("/list", name="admin_user_list")
	 * @Template()
	 * @param Request $request
	 * @return array
	 */
	public function listAction(Request $request)
	{
		$list = $this->getDoctrine()->getRepository('AppBundle:User')->findBy(
		    ['role' => 'ROLE_USER', 'deleted' => false]
        );

		if ( $request->query->get('export') ) {
            $delimiter = ",";
            $filename = "members_" . date('Y-m-d') . ".csv";

            //create a file pointer
            $f = fopen('php://memory', 'w');

            //set column headers
            $fields = array('ID', 'First name', 'Last name', 'Company', 'Email');
            fputcsv($f, $fields, $delimiter);

            //output each row of the data, format line as csv and write to file pointer
            foreach ($list as $row) {
                $lineData = array(
                    $row->getId(),
                    $row->getFirstName(),
                    $row->getLastName(),
                    $row->getCompany(),
                    $row->getEmail());

                fputcsv($f, $lineData, $delimiter);
            }

            //move back to beginning of file
            fseek($f, 0);

            //set headers to download file rather than displayed
            header('Content-Type: text/csv');
            header('Content-Disposition: attachment; filename="' . $filename . '";');

            //output all remaining data on a file pointer
            fpassthru($f);
            fclose($f);
            exit();
        }

		return [
		    'list' => $list,
		];
	}

	/**
	 * @Security("has_role('ROLE_ADMIN')")
	 * @Route("/view/{id}", name="admin_user_view")
	 * @Template()
	 * @param Request $request
	 * @return array
	 */
	public function viewAction(Request $request)
	{
		$user = $this->getDoctrine()->getRepository('AppBundle:User')
				->find($request->attributes->get('id'));
		$bankInformation = $this->getDoctrine()->getRepository('AppBundle:UserBankInformation')
				->findOneBy(['user' => $user->getId()]);
		$submitted = $this->getDoctrine()->getRepository('AppBundle:Prospect')
											->countByUserAndStatus($user->getId(), 'submitted');
		$closed = $this->getDoctrine()->getRepository('AppBundle:Prospect')
				->countByUserAndStatus($user->getId(), 'closed');
		$paid = $this->getDoctrine()->getRepository('AppBundle:Prospect')
				->countByUserAndPayoutStatus($user->getId(), 'paid');

        $pending = $this->getDoctrine()->getRepository('AppBundle:Prospect')
            ->countByUserAndPayoutStatus($user->getId(), 'pending');
        $earnedMonth = $this->getDoctrine()->getRepository('AppBundle:ProspectUserPayment')
            ->earnedInYearAndOrMonth($user, date('Y'), date('m'));

        $previousMonth = date( 'm', strtotime("first day of previous month"));
        $yearPreviousMonth = date( 'Y', strtotime("first day of previous month"));
        $earnedPreviousMonth = $this->getDoctrine()->getRepository('AppBundle:ProspectUserPayment')
            ->earnedInYearAndOrMonth($user, $yearPreviousMonth, $previousMonth);
        $earnedYear = $this->getDoctrine()->getRepository('AppBundle:ProspectUserPayment')
            ->earnedInYearAndOrMonth($user, date('Y'));

        $bankInformation = $this->getDoctrine()->getRepository('AppBundle:UserBankInformation')
            ->findOneBy(['user' => $user->getId()]);

        if ( ! $bankInformation ) {
            $bankInformation = new UserBankInformation();
            $bankInformation->setUser($user);
        }
        $formBankInformation = $this->createForm(BankInformationType::class, $bankInformation);

		return [
            'user' => $user,
            'previousMonth' => $previousMonth,
            'bankInformation' => $bankInformation,
            'status' => [
                'submitted' => $submitted,
                'closed'  => $closed,
                'paid'  => $paid
            ],
            'payout' => [
                'pending' => $pending
            ],
            'earned' => [
                'month' => $earnedMonth ?: 0,
                'previousMonth' => $earnedPreviousMonth ?: 0,
                'earnedYear' => $earnedYear ?: 0
            ],
            'bankInformation' => $bankInformation,
            'formBankInformation' => $formBankInformation->createView(),
		];
	}

    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("/update", name="admin_user_update")
     * @param Request $request
     * @return array|JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
	public function updateAction(Request $request) {

	    if ( $request->isMethod('post') ) {
	        $data = $request->request->all();
            $user = $this->getDoctrine()->getRepository('AppBundle:User')->find($data['id']);
            $user->setFirstName($data['firstName']);
            $user->setLastName($data['lastName']);
            $user->setEmail($data['email']);
            $user->setAddress($data['address']);
            $user->setCity($data['city']);
            $user->setState($data['state']);
            $user->setZip($data['zip']);
            $user->setPhone($data['phone']);
            $user->setContactPreference($data['contactPreference']);
            $user->setCompanyName($data['company']);
            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();

            $this->get('session')->getFlashBag()->add('success', "User information has been updated");
            return $this->redirectToRoute('admin_user_view', ['id' => $data['id']]);
        }
    }

    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("/update-bank-information", name="admin_user_update_information_bank")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function updateInformationBankAction(Request $request)
    {
        $data = $request->request->all();

        $bankInformation = $this->getDoctrine()->getRepository('AppBundle:UserBankInformation')
            ->findOneBy(['user' => $data['id']]);
        $user = $this->getDoctrine()->getRepository('AppBundle:User')->find($data['id']);
        if ( ! $bankInformation ) {
            $bankInformation = new UserBankInformation();
            $bankInformation->setUser($user);
        }

        $bankInformation->setTypeMethod($data['app_user_bank_information']['typeMethod']);
        $bankInformation->setInformationOne($data['app_user_bank_information']['informationOne']);
        $bankInformation->setInformationTwo($data['app_user_bank_information']['informationTwo']);
        $bankInformation->setInformationThree($data['app_user_bank_information']['informationThree']);

        $this->getDoctrine()->getManager()->persist($bankInformation);
        $this->getDoctrine()->getManager()->flush();

        $this->get('session')->getFlashBag()->add('success', "User bank information has been updated");
        return $this->redirectToRoute('admin_user_view', ['id' => $data['id']]);
    }

	/**
	 * @Security("has_role('ROLE_ADMIN')")
	 * @Route("/change-status/{id}", name="admin_user_change_status")
	 * @param Request $request
	 * @return array|JsonResponse
	 */
	public function changeStatusAction(Request $request)
	{
		$user = $this->getDoctrine()->getRepository('AppBundle:User')->find($request->attributes->get('id'));
		if ($user->isEnabled()) {
			$user->disabled();
		} else {
			$user->enabled();
		}

		$this->getDoctrine()->getManager()->persist($user);
		$this->getDoctrine()->getManager()->flush();
        #send mail activation account
        //$this->mailActivation($user);

		return new JsonResponse(['message' => 'Status change'], 200);
	}

    /**
     * @param User $user
     */
	protected function mailActivation(User $user)
	{
		$message = \Swift_Message::newInstance()
				->setSubject("Welcome to ITG Residual's Website")
				->setFrom(array( $this->getParameter('mail_company') => $this->getParameter('name_application') ))
				->setTo($user->getEmail())
				->setContentType("text/html")
				->setBody(
						$this->renderView(
								'@App/Emails/activation.html.twig',
								array('user' => $user)
						),
						'text/html'
				);

		$this->get('mailer')->send($message);
	}

  /**
   * @Security("has_role('ROLE_ADMIN')")
   * @Route("/{id}/delete", name="admin_user_delete")
   * @param Request $request
   * @return JsonResponse
   * @throws \Exception
   */
  public function remove(Request $request)
  {
      try {
          $user = $this->getDoctrine()->getRepository('AppBundle:User')
              ->find($request->attributes->get('id'));
//          $user->setDeleted(true);

          $this->getDoctrine()->getManager()->remove($user);
          $this->getDoctrine()->getManager()->flush();
          $result = ['code' => 200,'success' => 'User removed'];
      } catch (\Exception $e) {
          $result = [
              'code' => 400,
              'error' => $e->getMessage()
          ];
      }

      return new JsonResponse($result, 200);
  }
}
