<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Prospect;
use AppBundle\Form\ProspectType;
use AppBundle\Utils\NutshellApi;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class ReferralController
 * @package AppBundle\Controller
 * @Route("/referral")
 */
class ReferralController extends Controller
{
	/**
	 * @Security("has_role('ROLE_USER')")
	 * @Route("/list", name="app_referral_list")
	 * @Template()
	 */
	public function listAction(Request $request)
	{
	    $list = $this->getDoctrine()->getRepository('AppBundle:Prospect')
                                    ->loadByUser($this->getUser());

        return [
          'list' => $list
        ];
	}

	/**
	 * @Security("has_role('ROLE_USER')")
	 * @Route("/submission", name="app_referral_new")
	 * @Template()
	 */
	public function newAction(Request $request)
	{
        $prospect = new Prospect();
        $prospect->setUser($this->getUser());
        $prospect->setStatus('submitted');
        $form = $this->createForm(ProspectType::class, $prospect);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            $prospect = $form->getData();
	          $file = $prospect->getAttachment();

	          if ($file) {
		          $fileName = 'referral-' . $prospect->getId().'.'.$file->guessExtension();

		          // Move the file to the directory where brochures are stored
		          $file->move(
				          $this->getParameter('referrals_directory'),
				          $fileName
		          );

		          $prospect->setAttachment($fileName);
	          }

            $em = $this->getDoctrine()->getManager();
            $em->persist($prospect);
            $em->flush();

            $dataLead = [
                'name' => $prospect->getName(),
                'phone' => $prospect->getContactPhone(),
                'address' => [
                        "address_1" => $prospect->getAddress(),
                        "city" =>       $prospect->getCity(),
                        "state" => $prospect->getState(),
                        "postalCode" => $prospect->getZip()
                ],
                'notes' => $prospect->getNotes(),
                'contact' => [
                    'name' => $prospect->getContactFirstName() . ' ' . $prospect->getContactLastName(),
                    'phone' => $prospect->getContactPhone(),
                    'email' => $prospect->getContactEmail(),
                ]
            ];

            $lead = $this->createLead($dataLead);

            $prospect->setNutshellAccountId($lead['account']);
            $prospect->setNutshellLeadId($lead['lead']);
            $prospect->setNutshellContactId($lead['contact']);
            $em = $this->getDoctrine()->getManager();
            $em->persist($prospect);
            $em->flush();

            // send mail to administrator
            $this->mail($prospect);

            return $this->redirectToRoute('app_referral_list');
        }

		return ['form' => $form->createView(),];
	}

    /**
     * @param $lead
     * @return array
     */
	protected function createLead($lead) {
        $api = new NutshellApi($this->getParameter('nutshell_username'), $this->getParameter('nutshell_apikey'));

        $params = [
            'account' => ['name' => $lead['name'], 'phone' => $lead['phone'],
                'address' => [
                    'office' => [
                        'address_1'  => $lead['address']['address_1'],
                        'city'       => $lead['address']['city'],
                        'state'      => $lead['address']['state'],
                        'postalCode' => $lead['address']['postalCode'],
                    ],
                ],
            ]
        ];
        $newAccount = $api->newAccount($params);
        $account = $newAccount->id;

        $params = [
            'lead' => [
                'primaryAccount' => array('id' => $account),
                "description" => $lead['name'],
                "note" => $lead['notes'],
            ]];

        $newLead = $api->newLead($params);

        $contact = [
            "contact" => [
                "name" => $lead['contact']['name'],
                "phone" => $lead['contact']['phone'],
                "email" => $lead['contact']['email'],
                "accounts" => [[
                    'relationship' => 'Contact',
                    'id' => $account
                ]],
                "leads" => [[
                    'relationship' => 'Lead',
                    'id' => $newLead->id
                ]]
            ]
        ];

        $newContact = $api->newContact($contact);

        return [
            'account' => $account,
            'lead' => $newLead->id,
            'contact' => $newContact->id
        ];
    }

    /**
     * @Route("/edit/{id}", name="app_referral_edit")
     * @Template()
     */
    public function editAction(Request $request)
    {
        $prospect = $this->getDoctrine()->getRepository('AppBundle:Prospect')
                                        ->find($request->attributes->get('id'));

        $attachment = $prospect->getAttachment();
        $prospect->setAttachment(null);
        $form = $this->createForm(ProspectType::class, $prospect);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            $prospect = $form->getData();
            $data = $request->request->all();

            $file = $prospect->getAttachment();

            if ($file) {
                $fileName = 'referral-' . $prospect->getId().'.'.$file->guessExtension();

                // Move the file to the directory where brochures are stored
                $file->move(
                        $this->getParameter('referrals_directory'),
                        $fileName
                );

                $prospect->setAttachment($fileName);
            } else {
                $prospect->setAttachment($data['attachment-current']);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($prospect);
            $em->flush();

            if ( $prospect->getNutshellLeadId() ) {
                $nutshellIds = [
                    'account'   =>  $prospect->getNutshellAccountId(),
                    'lead'      =>  $prospect->getNutshellLeadId(),
                    'contact'   =>  $prospect->getNutshellContactId()
                ];

                $dataLead = [
                    'name' => $prospect->getName(),
                    'phone' => $prospect->getContactPhone(),
                    'address' => [
                        "address_1" => $prospect->getAddress(),
                        "city" =>       $prospect->getCity(),
                        "state" => $prospect->getState(),
                        "postalCode" => $prospect->getZip()
                    ],
                    'notes' => $prospect->getNotes(),
                    'contact' => [
                        'name' => $prospect->getContactFirstName() . ' ' . $prospect->getContactLastName(),
                        'phone' => $prospect->getContactPhone(),
                        'email' => $prospect->getContactEmail(),
                    ]
                ];

                $this->editLead($nutshellIds, $dataLead);
            }

            if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
                return $this->redirectToRoute('admin_referral_list');
            }

            return $this->redirectToRoute('app_referral_list');
        }

        return [
            'attachment' => $attachment,
            'form' => $form->createView(),
            'prospect' => $prospect
        ];
    }

    /**
     * @param $ids
     * @param $lead
     * @return array|bool
     */
    protected function editLead($ids, $lead) {
        $api = new NutshellApi($this->getParameter('nutshell_username'), $this->getParameter('nutshell_apikey'));

        $leadId      = $ids['lead'];
        $params      = array( 'leadId' => $leadId );
        $oldLead     = $api->call('getLead', $params);
        $rev         = $oldLead->rev;
        $oldContacts = $oldLead->contacts;

        // Build new contacts array containing the old contacts plus the one we want to add
        $contacts = array();
        foreach ($oldContacts as $contact) {
            $contacts[] = array(
                'id' => $contact-> id,
                'relationship' => $contact->relationship,
                'entityType' => 'Contacts',
                // entityType is required for updating multivalue keys when editing a lead.
                // this requirement will be removed in a future API release.
            );
        }
        $contacts[] = array(
            'relationship' =>'additional contact',
            'id'           => 17,
            'entityType' => 'Contacts',
            // entityType is required for updating multivalue keys when editing a lead.
            // this requirement will be removed in a future API release.
        );

        // edit the lead
        $params = array(
            'leadId' => $leadId,
            'rev'    => $rev,
            'lead'   => array(
                'confidence' => 75,
//                'contacts'   => $contacts,
                "description" => $lead['name'],
                "note" => $lead['notes']
            ),
        );
        $result = $api->editLead($params);
        $params = [
            'account' => ['name' => $lead['name'], 'phone' => $lead['phone'],
                'address' => [
                    'office' => [
                        'address_1'  => $lead['address']['address_1'],
                        'city'       => $lead['address']['city'],
                        'state'      => $lead['address']['state'],
                        'postalCode' => $lead['address']['postalCode'],
                    ],
                ],
            ]
        ];

//        $api->editAccount($ids['account'], null, $params);
        $params = [
            'lead' => [
                'primaryAccount' => array('id' => $ids['account']),
                "description" => $lead['name'],
                "note" => $lead['notes'],
            ]];

//        $api->editLead($ids['lead'], $params);

        $contact = [
            "contact" => [
                "name" => $lead['contact']['name'],
                "phone" => $lead['contact']['phone'],
                "email" => $lead['contact']['email'],
                "accounts" => [[
                    'relationship' => 'Contact',
                    'id' => $ids['account']
                ]],
                "leads" => [[
                    'relationship' => 'Lead',
                    'id' => $ids['lead']
                ]]
            ]
        ];

//        $api->editContact($ids['contact'], 'Updated Contact ' . date('d-m-Y'), $contact);

        return $result;
    }

	protected function mail(Prospect $prospect)
	{
		$message = \Swift_Message::newInstance()
				->setSubject('New ITG Referral added to ITG Residuals Website')
				->setFrom(array( $this->getParameter('mail_company') => $this->getParameter('name_application') ))
				->setTo($this->getParameter('mail_company'))
				//			    ->setTo($user->getEmail())
				->setContentType("text/html")
				->setBody(
						$this->renderView(
								'@App/Emails/Admin/new_referral.html.twig',
								array('referral' => $prospect, 'user' => $this->getUser()->getFullName())
						),
						'text/html'
				);
		if ( $prospect->getAttachment()) {
			$message->attach(\Swift_Attachment::fromPath($this->getParameter('referrals_directory') . '/' . $prospect->getAttachment() ));
		}

		$this->get('mailer')->send($message);
	}
}
