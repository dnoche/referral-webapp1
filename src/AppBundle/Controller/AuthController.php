<?php

namespace AppBundle\Controller;

use AppBundle\Form\ChangePasswordType;
use AppBundle\Form\Model\ChangePassword;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class AuthController extends Controller
{
    /**
     * @Route("/login", name="login")
     * @Template()
     * @param AuthenticationUtils $authenticationUtils
     * @return array|Response
     */
		public function loginAction(AuthenticationUtils $authenticationUtils)
		{
//				if (
//						true === $this->get('security.authorization_checker')->isGranted('ROLE_USER') ||
//						true === $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')
//				) {
//					$this->redirectToRoute('homepage');
//				}
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return [
            'last_username' => $lastUsername,
            'error'         => $error,
        ];
		}

		/**
		 * @Route("/change-password", name="security_change_password")
		 * @Template()
		 */
		public function changePasswordAction(Request $request)
		{
			$changePasswordModel = new ChangePassword();
			$em = $this->getDoctrine()->getManager();
			$form = $this->createForm(ChangePasswordType::class, $changePasswordModel);

			$form->handleRequest($request);

			if ($form->isSubmitted()) {
				if ($form->isValid()) {
					$user = $this->getUser(); //metemos como id la del usuario sacado de su sesion
					$encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
					$password = $encoder->encodePassword($changePasswordModel->getPassword(), $user->getSalt());
					$user->setPassword($password);
					$em->persist($user);
					$flush = $em->flush();
					if ($flush === null) {

						$this->get('session')->getFlashBag()->add('success', 'El usuario se ha editado correctamente');
						return $this->redirectToRoute("app_home"); //redirigimos la pagina si se incluido correctamete
					} else {
						$this->get('session')->getFlashBag()->add('error', 'Error al editar el password');
					}
				} else {
					$this->get('session')->getFlashBag()->add('error', 'Error al editar el password');
				}
			}

			return [
					'form' => $form->createView(),
			];
		}

    /**
     * @Route("/logout", name="security_logout")
     */
    public function logoutAction()
    {

    }

		/**
		 * @Route("/redirect", name="app_redirect")
		 * @return \Symfony\Component\HttpFoundation\RedirectResponse
		 */
		public function madeAction()
		{
			return $this->redirectToRoute('app_home');
		}

	public function changePasswordStep2Action(Request $request){

		if($this->getUser() != null){
			$this->get('security.token_storage')->setToken(null);
			$this->get('request')->getSession()->invalidate();
		}
		$em = $this->getDoctrine()->getManager();
		$correoIngresado = $request->request->get("txtCorreo");
		$usuario = $em->getRepository('UsuariosBundle:Usuario')->findOneBy(array('email'=>$correoIngresado));

		if($usuario == null){
			$mensaje = "No hemos podido encontrar un usuario asociado a la dirección de correo ingresada, por favor controle el correo ingresado.";
			$clase = "error";
		}else{
			$mensaje = "Hemos enviado un correo a la dirección: " . $usuario->getEmail() . " para que pueda restablecer su contraseña.";
			$clase = "success";
			$this->sendMailRequestNewPasswordAction($usuario);
		}

		return $this->render('UsuariosBundle:Login:changePassword_step_1.html.twig',
				array(
						'mensaje'=> $mensaje,
						'clase'=> $clase
				)
		);
	}

	public function sendMailRequestNewPasswordAction($usuario){
		$correo = $usuario->getEmail();
		$message = \Swift_Message::newInstance()
				->setSubject('¡Restablecimiento de contraseña!')
				->setFrom(array('admin@e-valuados.com' => $this->getParameter('name_application') ))
				->setTo($correo)
				->setContentType("text/html")
				->setBody($this->renderView('UsuariosBundle:mailTemplates:request_cambioPassword.html.twig', array(
						'idUsuario'=>$usuario->getId(),
						'correo' => $correo
				)));
		$this->get('mailer')->send($message);
	}
}
