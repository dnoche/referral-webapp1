<?php
namespace AppBundle\Form;

use AppBundle\Form\Model\ChangePassword;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class ChangePasswordType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
//			->add('oldPassword', PasswordType::class, array(
//				'label' => 'Ponga su password actual',
//			))
			->add('password', RepeatedType::class, [
				"required" => true,
				'type' => PasswordType::class,
				'invalid_message' => 'The two passwords must match',
				'first_options' => array('label' => 'New Password', 'attr' => ['class' => 'form-password form-control']),
				'second_options' => array('label' => 'Confirm the new password', 'attr' => ['class' => 'form-password form-control'])
			]);
	}

	public function setDefaultOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
				'csrf_protection' => false,
				'data_class' => ChangePassword::class,
		));
	}

	public function getBlockPrefix()
	{
		return 'change_password';
	}
}