<?php
namespace AppBundle\Form;

use AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('companyName', TextType::class, [
                'label' => 'Company name',
                'required' => false,
                'attr' => ['class' => 'form-control']
            ])
            ->add('firstName', TextType::class, [
                'label' => 'First name',
                'attr' => ['class' => 'form-control']
            ])
            ->add('lastName', TextType::class, [
                'label' => 'Last Name',
                'attr' => ['class' => 'form-control']
            ])
            ->add('address', TextType::class, [
                'label' => 'Address',
                'attr' => ['class' => 'form-control']
            ])
            ->add('city', TextType::class, [
                'label' => 'City',
                'attr' => ['class' => 'form-control']
            ])
            ->add('state', TextType::class, [
                'label' => 'State',
                'attr' => ['class' => 'form-control']
            ])
            ->add('zip', TextType::class, [
                'label' => 'ZIP',
                'attr' => ['class' => 'form-control']
            ])
            ->add('email', EmailType::class,[
                'attr' => ['class' => 'form-control']
            ])
            ->add('phone', TextType::class,[
                'attr' => ['class' => 'form-control']
            ])
            ->add('contactPreference', ChoiceType::class, [
                'label' => 'Preferred Contact Method',
                'attr' => ['class' => 'form-control'],
                'choices'  => [
                    'Phone' => 'phone',
                    'Email' => 'email'
                ],
            ])
            ->add('photo', FileType::class, [
                    'label' => 'Photo',
                    'required' => false,
                    'attr' => ['class' => 'form-control'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => false,
            'data_class' => User::class,
        ));
    }

		/**
		 * {@inheritdoc}
		 */
		public function getBlockPrefix()
		{
			return 'app_user_account';
		}
}