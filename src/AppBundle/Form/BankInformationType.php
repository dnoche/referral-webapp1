<?php
namespace AppBundle\Form;

use AppBundle\Entity\UserBankInformation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class BankInformationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('informationOne', TextType::class, [
                'attr' => ['class' => 'form-control']
            ])
            ->add('informationTwo', TextType::class, [
                'attr' => ['class' => 'form-control'],
		            'required' => false
            ])
		        ->add('informationThree', TextType::class, [
				        'attr' => ['class' => 'form-control'],
				        'required' => false
		        ])
            ->add('typeMethod', ChoiceType::class, [
                'label' => 'Payment Option',
                'attr' => ['class' => 'form-control'],
                'choices'  => [
                    'Check' => 'check',
                    'Direct Deposit' => 'deposit'
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => false,
            'data_class' => UserBankInformation::class,
        ));
    }

		/**
		 * {@inheritdoc}
		 */
		public function getBlockPrefix()
		{
			return 'app_user_bank_information';
		}
}