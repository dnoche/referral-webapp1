<?php

namespace AppBundle\Form;

use AppBundle\Entity\Prospect;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ProspectType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Company Name',
                'attr' => ['class' => 'form-control']
            ])
            ->add('employees', TextType::class, [
                'label' => '# of Employees',
                'attr' => ['class' => 'form-control']
            ])
            ->add('address', TextType::class, [
                'label' => 'Address',
                'attr' => ['class' => 'form-control']
            ])
            ->add('city', TextType::class, [
                'label' => 'City',
                'attr' => ['class' => 'form-control']
            ])
            ->add('state', TextType::class, [
                'label' => 'State',
                'attr' => ['class' => 'form-control']
            ])
            ->add('zip', TextType::class, [
                'label' => 'ZIP',
                'attr' => ['class' => 'form-control']
            ])
            ->add('contactFirstName', TextType::class, [
                'label' => 'Contact First Name',
                'attr' => ['class' => 'form-control']
            ])
            ->add('contactLastName', TextType::class, [
                'label' => 'Contact Last Name',
                'attr' => ['class' => 'form-control']
            ])
            ->add('contactTitle', TextType::class, [
                'label' => 'Contact Title',
                'attr' => ['class' => 'form-control']
            ])
            ->add('contactPreferredMethodContact', ChoiceType::class, [
                'label' => 'Preferred Contact Method',
                'attr' => ['class' => 'form-control'],
                'choices'  => [
                    'Phone' => 'phone',
                    'Email' => 'email'
                ],
            ])
            ->add('contactPhone', TextType::class, [
                'label' => 'Contact Phone',
                'attr' => ['class' => 'form-control']
            ])
            ->add('contactEmail', TextType::class, [
                'label' => 'Contact Email',
                'attr' => ['class' => 'form-control']
            ])
            ->add('customerType', ChoiceType::class, [
                'label' => 'Customer Type',
                'attr' => ['class' => 'form-control'],
                'choices'  => [
                    'Small Business' => 'small_business',
                    'Enterprise' => 'enterprise',
                    'Residential' => 'residential',
                ],
            ])
            ->add('permissionCall', CheckboxType::class, [
                'label' => 'Has Customer given permission to call?',
                'required' => false,
                'attr' => ['class' => 'custom-control-input', 'id' => 'permission']
            ])
            ->add('callBefore', CheckboxType::class, [
                'label' => 'Please call me before contacting customer',
                'required' => false,
                'attr' => ['class' => 'custom-control-input']
            ])
            ->add('alarmReferral', CheckboxType::class, [
                'label' => 'Alarm Referral',
                'required' => false,
                'attr' => ['class' => 'custom-control-input']
            ])
            ->add('dateOfBirth', TextType::class, [
                'label' => 'Date of Birth',
                'required' => false,
                'attr' => ['class' => 'form-control']
            ])
            ->add('socialSecurity', TextType::class, [
                'label' => 'Last 4 Digits Social Security:',
                'required' => false,
                'attr' => ['class' => 'form-control']
            ])
            ->add('doors', TextType::class, [
                'label' => 'Doors',
                'required' => false,
                'attr' => ['class' => 'form-control']
            ])
            ->add('windows', TextType::class, [
                'label' => 'Windows',
                'required' => false,
                'attr' => ['class' => 'form-control']
            ])
            ->add('notes', TextareaType::class, [
                'label' => 'Notes',
                'attr' => [
                		'placeholder' => 'Notes: (If you know what services the client is interested in, please let us know and write
below your comments) (Optional)',
		                'class' => 'form-control',
		                'rows' => 8
                ]
            ])
            ->add('attachment', FileType::class, [
                    'label' => 'Attachment file',
                    'required' => false,
                    'attr' => ['class' => 'form-control']
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Prospect::class,
            'csrf_protection' => false,
            'compound' => true,
        ));
    }
}